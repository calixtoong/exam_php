<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * API Controller
 * @author Calixto "Bhong" Ong II  <bhongong@gmail.com> 2019-02-04 10:41:47 PM
 */
class Api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Organization_model');
	}

	public function index()
	{
		header('HTTP/1.0 403 Forbidden');
		echo 'Forbidden access';
	}


	/**
	 * [organization description]
	 *  - endpoint that shows the organization structure. employee
	 * @return JSON encoded employees
	 */
	public function organization()
	{

		$response = array();

		$employees = $this->Organization_model->getEmployees();

		foreach($employees as $employee) {

			if ((int)$employee['employeeUnder'] > 0) {

				$employee['employeeUnder'] = $this->Organization_model->getReportsTo($employee['employeeNumber']);

			} else {

				$employee['employeeUnder'] = array();

			} //END if ((int)$employee['reportsTo'] > 0) {

			$response[] = $employee;

		} //END foreach($employees as $employee) {

		echo json_encode($response, true);

	} //END public function organization()


	/**
	 * offices API
	 * - endpoint that shows each offices contains which employee.`
	 * @return [type] [description]
	 */
	public function offices()
	{
		$response = array();

		$offices = $this->Organization_model->getOffices();

		foreach($offices as $office) {

			$office['employees'][] = $this->Organization_model->getOfficeEmployees($office['officeCode']);

			$response[] = $office;

		} //END foreach($offices as $office) {

		echo json_encode($response, true);

	}


	public function sales_report($employeeNumber=0)
	{
		$response = array();

		//get employees
		$employees = $this->Organization_model->getEmployeeSalesCommission($employeeNumber);
		foreach($employees as $employee) {

			//get productlines summary of each employee
			$productLines = $this->Organization_model->getEmployeeProductLineSales($employee['employeeNumber']);
			foreach($productLines as $productLine) {

				//get products summary under each productline
				$products = $this->Organization_model->getEmployeeProductSaleByProductLine($employee['employeeNumber'], $productLine['productLines']);
				foreach($products as $product) {

					$productLine['products'][] = $product;

				} //END foreach($products as $product) {

				$employee['productLines'][] = $productLine;

			} //END foreach($productLines as $productLine) {

			$response[] = $employee;

		} //END foreach($employees as $employee) {

		echo json_encode($response, true);
	}

}

/* End of file Api.php */
/* Location: ./application/controllers/Api.php */
