<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Organization_Model
 *
 * @author Calixto "Bhong" Ong II  <bhongong@gmail.com> 2019-02-04 10:41:47 PM
 */
class Organization_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * getEmployees 		Get list of all employees
	 * @return array 		multiple rows
	 */
	public function getEmployees()
	{
		$result = $this->db->query("SELECT employeeNumber, CONCAT(firstName,' ', lastName) AS `name`, jobTitle, reportsTo AS  `employeeUnder` FROM employees;");
		return ($result->num_rows()>0) ? $result->result_array() : array();
	}

	/**
	 * getReportsTo 		Look up Employee ID to check which Employee he/she is under (reporting to)
	 * @param  integer 		$employeeNumber Employee Number
	 * @return array        Single row employee record
	 */
	public function getReportsTo($employeeNumber = 0)
	{
		$result = $this->db->select('employeeNumber, CONCAT(firstName," ", lastName) AS `name`, jobTitle, reportsTo AS `employeeUnder`')
					->from('employees')
					->where(array('employeeNumber'=>$employeeNumber))
					->get();

		return ($result->num_rows()>0) ? $result->row_array() : array();
	}

	/**
	 * getOffices 			list of all offices
	 * @return array 		multiple rows
	 */
	public function getOffices()
	{
		$result = $this->db->select('officeCode, city')
					->from('offices')
					->get();

		return ($result->num_rows()>0) ? $result->result_array() : array();
	}

	/**
	 * getOfficeEmployees 				Get list of all employees under a certain office
	 * @param  integer 	$officeCode 	primary key of offices table
	 * @return array   					list of employees per office
	 */
	public function getOfficeEmployees($officeCode=0)
	{
		$result = $this->db->select('employeeNumber, CONCAT(firstName," ", lastName) AS `name`, jobTitle')
					->from('employees')
					->where(array('officeCode'=>$officeCode))
					->get();

		return ($result->num_rows()>0) ? $result->result_array() : array();
	}


	/**
	 * getEmployeeSalesCommission 		Get list of Employees and their total sales, commission for all Sales that are SHIPPED
	 * @param  integer $employeeNumber 	Employee Number
	 * @return array                   	multiple rows
	 */
	public function getEmployeeSalesCommission($employeeNumber=0)
	{
		//variable for WHERE CLAUSE (empty string if none)
		$where = '';

		//ensure employee number is numeric value
		$employeeNumber = (int)$employeeNumber;

		//if there is a passed Employee Number, assemble WHERE CLAUSE
		if ($employeeNumber>0) {
			$where = " AND employeeNumber=".$employeeNumber;
		}

		$result = $this->db->query("
								SELECT
									e.employeeNumber, CONCAT(e.firstName,' ', e.lastName) AS `name`, e.jobTitle, e.officeCode, l.city,
									FORMAT(SUM((d.quantityOrdered / SUBSTRING_INDEX(p.productScale, ':', -1))* d.priceEach), 2) AS `totalCommission`,
									SUM(d.quantityOrdered * d.priceEach) AS totalSales
									-- SUM((d.quantityOrdered / SUBSTRING_INDEX(p.productScale, ':', -1))* d.priceEach) AS `totalCommission`
								FROM orderdetails d
									LEFT JOIN orders o ON o.orderNumber = d.orderNumber
									LEFT JOIN products p ON d.productCode = p.productCode
									LEFT JOIN customers c ON c.customerNumber = o.customerNumber
									LEFT JOIN employees e ON e.employeeNumber = c.salesRepEmployeeNumber
									LEFT JOIN offices l ON e.officeCode = l.officeCode
								WHERE
									o.status = 'Shipped'
									$where
								GROUP BY e.employeeNumber
							;");

		return ($result->num_rows()>0) ? $result->result_array() : array();
	}

	/**
	 * getEmployeeProductLineSales 		Get summary of Employee Sales per Product Line
	 * @param  integer $employeeNumber 	Employee ID filter
	 * @return array                  	multiple rows
	 */
	public function getEmployeeProductLineSales($employeeNumber=0)
	{
		//ensure employee number is numeric value
		$employeeNumber = (int)$employeeNumber;

		$result = $this->db->query("
								SELECT
									p.productLine AS productLines,
									pl.textDescription,
									ROUND(SUM((d.quantityOrdered / SUBSTRING_INDEX(p.productScale, ':', -1))* d.priceEach), 2) AS `commission`,
									SUM(d.quantityOrdered) AS quantity,
									SUM(d.quantityOrdered * d.priceEach) AS `sales`
								FROM orderdetails d
									LEFT JOIN orders o ON o.orderNumber = d.orderNumber
									LEFT JOIN products p ON d.productCode = p.productCode
									LEFT JOIN productlines pl ON p.productLine = pl.productLine
									LEFT JOIN customers c ON c.customerNumber = o.customerNumber
									LEFT JOIN employees e ON e.employeeNumber = c.salesRepEmployeeNumber
								WHERE o.status = 'Shipped' AND e.employeeNumber = ?
								GROUP BY p.productLine
							;", array($employeeNumber));

		return ($result->num_rows()>0) ? $result->result_array() : array();
	}


	/**
	 * getEmployeeProductSaleByProductLine 	Get summary of Products Sold per Product Line for a certain Employee
	 * @param  integer $employeeNumber  	Employee ID filter
	 * @param  string  $productLine    		The Product Line Filter (Motorcycles, Planes)
	 * @return array                  		multiple rows
	 */
	public function getEmployeeProductSaleByProductLine($employeeNumber=0, $productLine='')
	{
		//ensure employee number is numeric value
		$employeeNumber = (int)$employeeNumber;

		$result = $this->db->query("
								SELECT
									p.productCode,
									p.productName,
									-- ROUND(SUM((d.quantityOrdered / SUBSTRING_INDEX(p.productScale, ':', -1))* d.priceEach), 2) AS `commission`,
									SUM(d.quantityOrdered) AS quantity,
									SUM(d.quantityOrdered * d.priceEach) AS `sales`,
									COUNT(c.customerNumber) AS numberOfCustomerBought
								FROM orderdetails d
									LEFT JOIN orders o ON o.orderNumber = d.orderNumber
									LEFT JOIN products p ON d.productCode = p.productCode
									LEFT JOIN productlines pl ON p.productLine = pl.productLine
									LEFT JOIN customers c ON c.customerNumber = o.customerNumber
									LEFT JOIN employees e ON e.employeeNumber = c.salesRepEmployeeNumber
								WHERE o.status = 'Shipped' AND e.employeeNumber = ? AND p.productLine = ?
								GROUP BY p.productCode
							;", array($employeeNumber, $productLine));
		return ($result->num_rows()>0) ? $result->result_array() : array();
	}
}

/* End of file Organization_model.php */
/* Location: ./application/models/Organization_model.php */
